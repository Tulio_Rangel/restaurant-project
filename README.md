
# Restaurant project (API Delilah Restó)

Project in which you can as a restaurant administrator, add, modify or remove products, see your customers orders and modify their status.
As a user you can see the available products, create an account, add or remove products from your order, choose payment methods, etc.

## Resources
- Node.js
- Express.js
- Swagger
- Bcrypt
- JWT
- redis
- Mongoose
- MongoDB Atlas

## Installation

 Clone the [repository](https://gitlab.com/Tulio_Rangel/restaurant-project) and install the dependencies
 running the next command in the console.

```bash
npm install
```

## Usage

1. Set your database:

- Go to file db.js in the config folder.

### For using MongoDB Atlas
- Change process.env.USERDB for your user in MongoDB Atlas.
- Change process.env.PASSDB for your password in MongoDB Atlas.
- Change process.env.DBNAME for you databe name in MongoDB Atlas

### For using local storage
- Change the urlDB for 'mongodb://localhost:27017/yourDBName'

2. Run the next command in the console:

```bash
node src/app.js
```
or
```bash
nodemon src/app.js
```
3. Find Swagger [documentacion](http://localhost:3000/api-docs/)

## Credentials:

When you run the project these users should be created automatically. 

- admin is an admin user.
- test is a normal user

|  username  | password |
|------------|----------|
|   admin    |   admin  |
|   test     |   test   |

- Note 1: All new users are created with property isAdmin as false by default.
- Note 2: All new users are created with property isActive as true by default, admins can change that property to suspend an user if is necesary.