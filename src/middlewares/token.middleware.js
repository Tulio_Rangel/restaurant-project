async function Verificar (req, res, next){
    const bearerHeader = req.headers['authorization'];
    const bearer = bearerHeader.split(" ");
    const token = bearer[1];

    if(!token){
        return res.status(401).send({ auth:false, message:"Token is missing" })
    } else{
        next();
    }
};