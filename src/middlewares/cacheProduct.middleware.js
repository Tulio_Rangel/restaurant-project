const clientRedis = require("../config/redis");

const cacheProducts = (req, res, next) => {
    try{
        clientRedis.get('products', (err, arrayProducts) => {
            if (err) throw err;
            if (arrayProducts){
             res.json(JSON.parse(arrayProducts));
         } else{
               next();
            }
        });
    } catch (e) {
        getDefaultResponse(res, 500, 1)
    }
};

module.exports = cacheProducts;