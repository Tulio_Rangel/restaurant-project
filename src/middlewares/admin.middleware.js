const User = require("../models/user.model");
const jwt = require("jsonwebtoken");
const port = process.env.PORT || 3000;

const adminAuth = async (req, res, next) => {
    const bearerHeader = req.headers['authorization'];
    const bearer = bearerHeader.split(" ");
    const token = bearer[1];

    const decoded = await jwt.verify(token, process.env.SECRET);
    const adminUser = await User.findById(decoded.id);
    if(adminUser.isAdmin === true){
        next();
    } else{
        return res.status(401).send({ auth: false, message: "You are not an admin" });
    }
};


module.exports = adminAuth;