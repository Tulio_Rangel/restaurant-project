const express = require("express");
const router = express.Router();

const { AllOrders, newOrder, doOrder, updateOrder, deleteOrder } = require("../controllers/order.controller");

const adminAuth = require("../middlewares/admin.middleware");

router.get("/", AllOrders);

router.post("/createOrder", newOrder);

router.post("/doOrder/:id", doOrder);

router.put("/updateOrder/:id", updateOrder);

router.delete("/delete/:id", deleteOrder);

module.exports = router;