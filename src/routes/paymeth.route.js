const express = require("express");
const router = express.Router();

const adminAuth = require("../middlewares/admin.middleware");

const { CeroPayMethod, AllPayMethods, NewPayMethod, UpdatePayMethod, DeletePayMethod } = require("../controllers/pay.controller");

router.get("/", AllPayMethods, adminAuth);

router.post("/", NewPayMethod, adminAuth);

router.put("/:id", UpdatePayMethod, adminAuth);

router.delete("/:id", DeletePayMethod, adminAuth);

module.exports = router;