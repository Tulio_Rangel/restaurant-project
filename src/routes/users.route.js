const express = require("express");
const router = express.Router();

const adminAuth = require("../middlewares/admin.middleware");
const verify = require("../middlewares/token.middleware");

const { FirstsUsers, AllUsers, NewUser, DeleteUser, Login, updateUser } = require("../controllers/user.controller");


router.get("/", AllUsers, adminAuth);

router.post("/signup", NewUser);

router.delete("/:id", DeleteUser, adminAuth);

router.put("/:id", updateUser,adminAuth);

router.post("/login", Login);

module.exports = router;