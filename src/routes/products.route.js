const express = require("express");
const router = express.Router();

const adminAuth = require("../middlewares/admin.middleware");
const cacheProducts = require("../middlewares/cacheProduct.middleware");

const { CeroProducts, NewProduct, UpdateProduct, DeleteProduct, AllProducts } = require("../controllers/product.controller");

router.get("/", cacheProducts, AllProducts);

router.post("/", NewProduct, adminAuth);

router.put("/:id", UpdateProduct, adminAuth);

router.delete("/:id", DeleteProduct, adminAuth);

module.exports = router;