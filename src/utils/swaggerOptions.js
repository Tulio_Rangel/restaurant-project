const swaggerOptions = {
    definition: 
      {
        "openapi": "3.0.0",
        "info": {
          "title": "Delilah Restó",
          "description": "API for a Restaurant, you can create user, edit products, do orders",
          "version": "0.2.5"
        },
        "servers": [
          {
            "url": "http://localhost:3000",
            "description": "Local Server"
          }
        ],
        "tags": [
          {
            "name": "Users",
            "description": "All about user"
          },
          {
            "name": "Products",
            "description": "All about products"
          },
          {
            "name": "Orders",
            "description": "All about orders"
          },
          {
            "name": "PaymentMethods",
            "description": "All about payment methods"
          }
        ],
        "paths": {
          "/users": {
            "get": {
              "tags": [
                "Users"
              ],
              "summary": "Get all users in the database",
              "description": "Get all users in the database",
              "responses": {
                "200": {
                  "description": "Ok",
                  "content": {
                    "application/json": {
                      "schema": {
                        "$ref": "#/components/schemas/User"
                      }
                    }
                  }
                }
              }
            }
          },
          "/users/signup": {
            "post": {
              "tags": [
                "Users"
              ],
              "summary": "Create a new user",
              "description": "Create a new user",
              "security": [],
              "requestBody": {
                "content": {
                  "application/json": {
                    "schema": {
                      "$ref": "#/components/schemas/User"
                    }
                  }
                }
              },
              "responses": {
                "400": {
                  "description": "Bad Request",
                  "content": {
                    "application/json": {
                      "schema": {
                        "type": "object",
                        "properties": {
                          "err": {
                            "type": "string",
                          }
                        }
                      }
                    }
                  }
                },
                "201": {
                  "description": "Created",
                  "content": {
                    "application/json": {
                      "schema": {
                        "type": "object",
                        "properties": {
                          "msg": {
                            "type": "string",
                          }
                        }
                      }
                    }
                  }
                },
                "400": {
                  "description": "Bad Request",
                  "content": {
                    "application/json": {
                      "schema": {
                        "type": "object",
                        "properties": {
                          "msg": {
                            "type": "string",
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
          },
          "/users/login": {
            "post": {
              "tags": [
                "Users"
              ],
              "summary": "Users can log in",
              "description": "Login",
              "requestBody": {
                "content": {
                  "application/json": {
                    "schema": {
                      "$ref": "#/components/schemas/UserLogin"
                    }
                  }
                }
              },
              "responses": {
                "404": {
                  "description": "Not Found",
                  "content": {
                    "application/json": {
                      "schema": {
                        "type": "object",
                        "properties": {
                          "err": {
                            "type": "string",
                          }
                        }
                      }
                    }
                  }
                },
                "401": {
                  "description": "Unauthorized",
                },
                "200": {
                  "description": "Ok",
                  "content": {
                    "application/json": {
                      "schema": {
                        "type": "object",
                        "properties": {
                          "msg": {
                            "type": "string",
                          }
                        }
                      }
                    }
                  }
                },
                "400": {
                  "description": "Bad Request",
                  "content": {
                    "application/json": {
                      "schema": {
                        "type": "object",
                        "properties": {
                          "err": {
                            "type": "string",
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
          },
          "/users/{id}": {
            "delete": {
              "tags": [
                "Users"
              ],
              "summary": "Admins can delete users",
              "description": "Admins can delete users",
              "parameters": [
                {
                  "in": "path",
                  "name": "id",
                  "description": "User's ID",
                  "required": true,
                  "schema": {
                    "type": "string",
                    "example": "600b365c79bdd616403fc73b"
                  }
                }
              ],
              "responses": {
                "200": {
                  "description": "Ok",
                  "content": {
                    "application/json": {
                      "schema": {
                        "type": "object",
                        "properties": {
                          "msg": {
                            "type": "string",
                          }
                        }
                      }
                    }
                  }
                },
                "400": {
                  "description": "Bad Request",
                  "content": {
                    "application/json": {
                      "schema": {
                        "type": "object",
                        "properties": {
                          "err": {
                            "type": "string",
                          }
                        }
                      }
                    }
                  }
                },
                "401": {
                  "description": "Unauthorized",
                }
              }
            }
          },
          "/products": {
            "get": {
              "tags": [
                "Products"
              ],
              "summary": "All products in the database",
              "description": "All products in the database",
              "responses": {
                "200": {
                  "description": "Ok",
                  "content": {
                    "application/json": {
                      "schema": {
                        "$ref": "#/components/schemas/Product"
                      }
                    }
                  }
                }
              }
            }
          },
          "/products": {
            "post": {
              "tags": [
                "Products"
              ],
              "summary": "Admins can create products",
              "description": "Admins can create products",
              "requestBody": {
                "content": {
                  "application/json": {
                    "schema": {
                      "$ref": "#/components/schemas/Product"
                    }
                  }
                }
              },
              "responses": {
                "400": {
                  "description": "Bad Request",
                  "content": {
                    "application/json": {
                      "schema": {
                        "type": "object",
                        "properties": {
                          "err": {
                            "type": "string",
                          }
                        }
                      }
                    }
                  }
                },
                "201": {
                  "description": "Created",
                  "content": {
                    "application/json": {
                      "schema": {
                        "type": "object",
                        "properties": {
                          "msg": {
                            "type": "string",
                          }
                        }
                      }
                    }
                  }
                },
                "400": {
                  "description": "Bad Request",
                  "content": {
                    "application/json": {
                      "schema": {
                        "type": "object",
                        "properties": {
                          "err": {
                            "type": "string",
                          }
                        }
                      }
                    }
                  }
                },
                "401": {
                  "description": "Unauthorized",
                }
              }
            }
          },
          "/products/{id}": {
            "put": {
              "tags": [
                "Products"
              ],
              "summary": "Admins can modify products",
              "description": "Admins can modify products",
              "parameters": [
                {
                  "in": "path",
                  "name": "id",
                  "required": true,
                  "schema": {
                    "type": "string",
                    "example": "600b365c79bdd616403fc73a"
                  }
                }
              ],
              "requestBody": {
                "content": {
                  "application/json": {
                    "schema": {
                      "$ref": "#/components/schemas/Product"
                    }
                  }
                }
              },
              "responses": {
                "200": {
                  "description": "Ok",
                  "content": {
                    "application/json": {
                      "schema": {
                        "type": "object",
                        "properties": {
                          "msg": {
                            "type": "string",
                          }
                        }
                      }
                    }
                  }
                },
                "400": {
                  "description": "Bad Request",
                  "content": {
                    "application/json": {
                      "schema": {
                        "type": "object",
                        "properties": {
                          "err": {
                            "type": "string",
                          }
                        }
                      }
                    }
                  }
                },
                "401": {
                  "description": "Unauthorized",
                }
              }
            }
          },
          "/products/{id}": {
            "delete": {
              "tags": [
                "Products"
              ],
              "summary": "Admins can delete products",
              "description": "Admins can delete products",
              "parameters": [
                {
                  "in": "path",
                  "name": "id",
                  "description": "Product's ID",
                  "required": true,
                  "schema": {
                    "type": "string",
                    "example": "600b365c79bdd616403fc73b"
                  }
                }
              ],
              "responses": {
                "200": {
                  "description": "Ok",
                  "content": {
                    "application/json": {
                      "schema": {
                        "type": "object",
                        "properties": {
                          "msg": {
                            "type": "string",
                          }
                        }
                      }
                    }
                  }
                },
                "400": {
                  "description": "Bad Request",
                  "content": {
                    "application/json": {
                      "schema": {
                        "type": "object",
                        "properties": {
                          "err": {
                            "type": "string",
                          }
                        }
                      }
                    }
                  }
                },
                "401": {
                  "description": "Unauthorized",
                }
              }
            }
          },
          "/orders": {
            "get": {
              "tags": [
                "Orders"
              ],
              "summary": "Admins and users can see the orders",
              "description": "Admins and users can see the orders",
              "responses": {
                "200": {
                  "description": "Ok",
                  "content": {
                    "application/json": {
                      "schema": {
                        "$ref": "#/components/schemas/Order"
                      }
                    }
                  }
                },
                "400": {
                  "description": "Bad Request",
                  "content": {
                    "application/json": {
                      "schema": {
                        "type": "object",
                        "properties": {
                          "err": {
                            "type": "string",
                          }
                        }
                      }
                    }
                  }
                },
                "401": {
                  "description": "Unauthorized",
                }
              }
            }
          },
          "/orders/createOrder": {
            "post": {
              "tags": [
                "Orders"
              ],
              "summary": "To start an order",
              "description": "To start an order",
              "responses": {
                "201": {
                  "description": "Created",
                  "content": {
                    "application/json": {
                      "schema": {
                        "type": "object",
                        "properties": {
                          "msg": {
                            "type": "string",
                          }
                        }
                      }
                    }
                  }
                },
                "401": {
                  "description": "Unauthorized",
                }
              }
            }
          },
          "/orders/doOrder/{id}": {
            "post": {
              "tags": [
                "Orders"
              ],
              "summary": "To finish ordering",
              "description": "To finish ordering",
              "parameters": [
                {
                  "in": "path",
                  "name": "id",
                  "required": true,
                  "schema": {
                    "type": "string",
                    "example": "600b365c79bdd616403fc73a"
                  }
                }
              ],
              "requestBody": {
                "content": {
                  "application/json": {
                    "schema": {
                      "$ref": "#/components/schemas/Order"
                    }
                  }
                }
              },
              "responses": {
                "201": {
                  "description": "Created",
                  "content": {
                    "application/json": {
                      "schema": {
                        "type": "object",
                        "properties": {
                          "msg": {
                            "type": "string",
                          }
                        }
                      }
                    }
                  }
                },
                "400": {
                  "description": "Bad Request",
                  "content": {
                    "application/json": {
                      "schema": {
                        "type": "object",
                        "properties": {
                          "err": {
                            "type": "string",
                          }
                        }
                      }
                    }
                  }
                },
                "400": {
                  "description": "Bad Request",
                  "content": {
                    "application/json": {
                      "schema": {
                        "type": "object",
                        "properties": {
                          "err": {
                            "type": "string",
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
          },
          "/orders/updateOrder/{id}": {
            "put": {
              "tags": [
                "Orders"
              ],
              "summary": "Users can edit the order if it's not confirmed yet",
              "description": "Users can edit the order if it's not confirmed yet",
              "parameters": [
                {
                  "in": "path",
                  "name": "id",
                  "required": true,
                  "schema": {
                    "type": "string",
                    "example": "600b365c79bdd616403fc73a"
                  }
                }
              ],
              "requestBody": {
                "content": {
                  "application/json": {
                    "schema": {
                      "$ref": "#/components/schemas/Order"
                    }
                  }
                }
              },
              "responses": {
                "200": {
                  "description": "Ok",
                  "content": {
                    "application/json": {
                      "schema": {
                        "type": "object",
                        "properties": {
                          "msg": {
                            "type": "string",
                          }
                        }
                      }
                    }
                  }
                },
                "400": {
                  "description": "Bad Request",
                  "content": {
                    "application/json": {
                      "schema": {
                        "type": "object",
                        "properties": {
                          "err": {
                            "type": "string",
                          }
                        }
                      }
                    }
                  }
                },
                "400": {
                  "description": "Bad Request",
                  "content": {
                    "application/json": {
                      "schema": {
                        "type": "object",
                        "properties": {
                          "err": {
                            "type": "string",
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
          },
          "/orders/delete/{id}": {
            "delete": {
              "tags": [
                "Orders"
              ],
              "summary": "Users can delete an order",
              "description": "Users can delete an order",
              "parameters": [
                {
                  "in": "path",
                  "name": "id",
                  "description": "Order's ID",
                  "required": true,
                  "schema": {
                    "type": "string",
                    "example": "600b365c79bdd616403fc73b"
                  }
                }
              ],
              "responses": {
                "200": {
                  "description": "Ok",
                  "content": {
                    "application/json": {
                      "schema": {
                        "type": "object",
                        "properties": {
                          "msg": {
                            "type": "string",
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
          },
          "/payMethods": {
            "get": {
              "tags": [
                "PaymentMethods"
              ],
              "summary": "Admins can see payment methods",
              "description": "Admins can see payment methods",
              "responses": {
                "200": {
                  "description": "Ok",
                  "content": {
                    "application/json": {
                      "schema": {
                        "$ref": "#/components/schemas/PaymentMethod"
                      }
                    }
                  }
                },
                "401": {
                  "description": "Unauthorized",
                }
              }
            }
          },
          "/payMethods": {
            "post": {
              "tags": [
                "PaymentMethods"
              ],
              "summary": "Admins can create new payment methods",
              "description": "Admins can create new payment methods",
              "requestBody": {
                "content": {
                  "application/json": {
                    "schema": {
                      "$ref": "#/components/schemas/PaymentMethod"
                    }
                  }
                }
              },
              "responses": {
                "400": {
                  "description": "Bad Request",
                  "content": {
                    "application/json": {
                      "schema": {
                        "type": "object",
                        "properties": {
                          "err": {
                            "type": "string",
                          }
                        }
                      }
                    }
                  }
                },
                "201": {
                  "description": "Created",
                  "content": {
                    "application/json": {
                      "schema": {
                        "type": "object",
                        "properties": {
                          "msg": {
                            "type": "string",
                          }
                        }
                      }
                    }
                  }
                },
                "400": {
                  "description": "Bad Request",
                  "content": {
                    "application/json": {
                      "schema": {
                        "type": "object",
                        "properties": {
                          "err": {
                            "type": "string",
                          }
                        }
                      }
                    }
                  }
                },
                "401": {
                  "description": "Unauthorized",
                }
              }
            }
          },
          "/payMethods/{id}": {
            "put": {
              "tags": [
                "PaymentMethods"
              ],
              "summary": "Admins can modify payment methods",
              "description": "Admins can modify payment methods",
              "parameters": [
                {
                  "in": "path",
                  "name": "id",
                  "required": true,
                  "schema": {
                    "type": "string",
                    "example": "600b365c79bdd616403fc73b"
                  }
                }
              ],
              "requestBody": {
                "content": {
                  "application/json": {
                    "schema": {
                      "$ref": "#/components/schemas/PaymentMethod"
                    }
                  }
                }
              },
              "responses": {
                "200": {
                  "description": "Ok",
                  "content": {
                    "application/json": {
                      "schema": {
                        "type": "object",
                        "properties": {
                          "msg": {
                            "type": "string",
                          }
                        }
                      }
                    }
                  }
                },
                "400": {
                  "description": "Bad Request",
                  "content": {
                    "application/json": {
                      "schema": {
                        "type": "object",
                        "properties": {
                          "err": {
                            "type": "string",
                          }
                        }
                      }
                    }
                  }
                },
                "401": {
                  "description": "Unauthorized",
                }
              }
            }
          },
          "/payMethods/{id}": {
            "delete": {
              "tags": [
                "PaymentMethods"
              ],
              "summary": "Admins can delete payment methods",
              "description": "Admins can delete payment methods",
              "parameters": [
                {
                  "in": "path",
                  "name": "id",
                  "description": "Paymethod's ID",
                  "required": true,
                  "schema": {
                    "type": "string",
                    "example": "600b365c79bdd616403fc73b"
                  }
                }
              ],
              "responses": {
                "200": {
                  "description": "Ok",
                  "content": {
                    "application/json": {
                      "schema": {
                        "type": "object",
                        "properties": {
                          "msg": {
                            "type": "string",
                            "example": "Payment Method was deleted"
                          }
                        }
                      }
                    }
                  }
                },
                "401": {
                  "description": "Unauthorized",
                }
              }
            }
          }
        },
        "security": [
          {
            "bearerAuth": []
          }
        ],
        "components": {
          "securitySchemes": {
            "bearerAuth": {
              "type": "http",
              "scheme": "bearer"
            }
          },
          "schemas": {
            "User": {
              "type": "object",
              "required": [
                "username",
                "fullname",
                "password",
                "email",
                "phone",
                "address"
              ],
              "properties": {
                "username": {
                  "type": "string",
                  "example": "theone"
                },
                "fullname": {
                  "type": "string",
                  "example": "Pedro Perez"
                },
                "password": {
                  "type": "string",
                  "example": "mypassword"
                },
                "email": {
                  "type": "strin",
                  "example": "myemail@mail.com"
                },
                "phone": {
                  "type": "string",
                  "example": "3120008976"
                },
                "address": {
                  "type": "string",
                  "example": "La casita embrujada 123"
                }
              }
            },
            "UserLogin": {
              "type": "object",
              "required": [
                "email",
                "password"
              ],
              "properties": {
                "email": {
                  "type": "string",
                  "example": "myemail@mail.com"
                },
                "password": {
                  "type": "string",
                  "example": "mysuperpassword"
                }
              }
            },
            "Product": {
              "type": "object",
              "required": [
                "productName",
                "description",
                "price"
              ],
              "properties": {
                "productName": {
                  "type": "string",
                  "example": "Cheaps"
                },
                "description": {
                    "type": "string",
                    "example": "Description here"
                },
                "precio": {
                  "type": "number",
                  "example": 3000
                }
              }
            },
            "PaymenMethod": {
              "type": "object",
              "required": [
                "payMethod"
              ],
              "properties": {
                "payMethod": {
                  "type": "string",
                  "example": "Nequi"
                }
              }
            },
            "Order": {
              "type": "object",
              "required": [
                "products",
                "quantities",
                "payMethod",
                "status"
              ],
              "properties": {
                "products": {
                  "type": "array",
                  "items": {},
                  "example": ["Cheese Burger","Coke"],
                },
                "quantities": {
                  "type": "array",
                  "items": {},
                  "example": [3,2],
                },
                "payMethod": {
                  "type": "string",
                  "example": "Nequi"
                },
                "status": {
                  "type": "string",
                  "example": "Pending"
                }
              }
            }
          }
        }
      },
    apis: ['./src/routes*.js']
  };

module.exports = swaggerOptions;