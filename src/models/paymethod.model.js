const mongoose = require("mongoose");
const { Schema } = mongoose;

const paySchema = new Schema({
    payMethod: {
        type: String,
        required: true
    }
});

const Pay = mongoose.model("Pay", paySchema);

module.exports = Pay;