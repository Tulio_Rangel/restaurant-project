const mongoose = require("mongoose");
const { Schema } = mongoose;

const orderSchema = new Schema({
    username: {
        type: String,
        required: true
    },
    address: {
        type: String,
        required: true
    },
    order:[{
        products: {
            type: Array,
            required: true
        },
        quantities: {
            type: Array,
            required: true
        },
        payMethod: {
            type: String,
            required: true
        },
        status: {
            type: String,
            required: true,
            default: "Pending"
        },
        comments: {
            type: String,
            required: false
        },
        totalPrice: {
            type: Number
        }
    }]
});

const Order = mongoose.model("Order", orderSchema);

module.exports = Order;