const request = require("supertest");
const app = require("../app");

it('respond with json contaning a list of all users', (done) => {
    request(app)
        .get('/users')
        .set('application/json')
        .expect('Content-Type', /json/)
        .expect(200, done)
        .end(function(err, res) {
            if (err) throw err;
        });
});