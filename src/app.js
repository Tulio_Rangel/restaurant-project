const express = require("express");
const expressJWT = require("express-jwt");
const helmet = require("helmet");
const port = process.env.PORT || 3000;

const swaggerJsDoc = require("swagger-jsdoc");
const swaggerUI = require("swagger-ui-express");

require('dotenv').config()

const mongoose = require("mongoose");
require("./config/db");

const usersRoute = require("./routes/users.route");
const productsRoute = require("./routes/products.route");
const payMethodsRoute = require("./routes/paymeth.route");
const ordersRoute = require("./routes/orders.route");

const swaggerOptions = require("./utils/swaggerOptions");

const app = express();
app.use(helmet());
app.use(express.json());

const swaggerSpecs = swaggerJsDoc(swaggerOptions);
app.use("/api-docs", swaggerUI.serve, swaggerUI.setup(swaggerSpecs));

app.set("port", process.env.PORT || 3000);

app.use(expressJWT({
    secret: process.env.SECRET,
    algorithms: ['HS256']
}).unless({
    path: ["/users"]
})
);

app.use("/users", usersRoute);
app.use("/products", productsRoute);
app.use("/paymethods", payMethodsRoute);
app.use("/orders", ordersRoute);

app.listen(3000, () => { console.log("Everything is good in port 3000 :D") });

export default app;