const User = require("../models/user.model");
const jwt = require("jsonwebtoken");
const port = process.env.PORT || 3000;

const FirstsUsers = async (req, res) => {
    try {
        const count = await User.estimatedDocumentCount();
        if (count > 0) return;

        const initialUsers = Promise.all([
            new User ({username: "admin", fullname: "Admin", password: "admin", email: "admin@gmail.com", phone: "+57 3001234567", address: "The end of the world", isAdmin: true}).save(),
            new User ({username: "test", fullname: "Test User", password: "test", email: "test@gmail.com", phone: "+57 3210003456", address: "Mordor", isAdmin: false}).save()
        ]);
        res.json(initialUsers);
    } catch (error) {
        return res.status(500).json(error.details[0].message);
    }
};

const AllUsers = async (req, res) => {
    try {
        const userArrayDB = await User.find();
        res.json(userArrayDB);
    } catch (error) {
        return res.status(500).json(error.details[0].message);
    }
};

const NewUser = async (req, res) => {
    try {
        const { username, fullname, password, email, phone, address, isAdmin, isActive } = req.body;
        if (username && fullname && password && email && phone && address){
            const validateEmail = User.findOne({ email });
            if (validateEmail){
                res.status(400).json({message: "The email is already resgistered"});
            } else{
                const user = new User({
                    username,
                    fullname,
                    password,
                    email,
                    phone,
                    address,
                    isAdmin,
                    isActive
                });
                user.password = await user.crypt(password);
                await user.save();
                const token = jwt.sign({ id: user.id }, process.env.SECRET, {
                    expiresIn: 60 * 60 * 24,
                });
                res.status(201).json({ auth: true, token });
            }
        } else{
            res.status(204).json({message: "Some data are missing"});
        }
    } catch (error) {
        return res.status(500).json(error.details[0].message);
    }
};

const DeleteUser = async (req, res) => {
    try {
        const { id } = req.params;
        if (id){
            await User.findByIdAndDelete(id);
            res.status(200).json({message: "The user was deleted"});
        } else{
            res.status(204).json({message: "Some data are missing"});
        }
    } catch (error) {
        return res.status(500).json(error.details[0].message);
    }
};

const Login = async (req, res) => {
    try {
        const { email, password } = req.body;
        if (email && password){
            const client = await User.findOne({ email: req.body.email });
            if (!client){
                return res.status(404).send("User is not registered");
            }
            const signal = await client.comparePassword(req.body.password, client.password);
            if (!signal){
                return res.status(401).send({ auth: false, token: null });
            }
            const token = jwt.sign({ id: user._id }, process.env.SECRET, {
                expiresIn: 60 * 60 * 24,
            });
            res.status(200).json({ auth: true, token });
        } else {
            res.status(204).json({message: "Some data are missing"});
        }
    } catch (error) {
        return res.status(500).json(error.details[0].message);
    }
};

const updateUser = async (req, res) => {
    try {
        const { username, fullname, password, email, phone, address, isAdmin, isActive } = req.body;
        if(isActive){
            const { id } = req.params;
            const updates = {...req.body};
            const options = { new: true };
            await User.findByIdAndUpdate(id, updates, options);
            res.status(200).json({message: "The user was updated"});
        } else{
            res.status(204).json({message: "Some data are missing"});
        }
    } catch (error) {
        return res.status(500).json(error.details[0].message);
    }
};

module.exports = { FirstsUsers, AllUsers, NewUser, DeleteUser, Login, updateUser };