const Order = require("../models/order.model");
const { Price } = require("../controllers/price.controller");
const User = require("../models/user.model");
const jwt = require("jsonwebtoken");
const port = process.env.PORT || 3000;

const AllOrders = async (req, res) => {
    try {
        const orders = Order.find();
        if(orders){
            res.status(200).json(orders);
        } else{
            res.status(204).json({message: "Some data are missing"});
        }
    } catch (error) {
        return res.status(500).json(error.details[0].message);
    }
};

const newOrder = async (req, res) => {
    try {
        const bearerHeader = req.headers['authorization'];
        if(bearerHeader){
            const bearer = bearerHeader.split(" ");
            const token = bearer[1];

            const decode = await jwt.verify(token, process.env.SECRET);
            const id = decode.id;
            const user = await User.findById(id);

            const client = user.fullname;
            const address = user.address;

            const startOrder = new Order({ client, address });
            await startOrder.save();
            res.status(201).json({message: "Order started"});
        } else{
            res.status(401).send({ auth: false, message: "Token is missing" });
        }
    } catch (error) {
        return res.status(500).json(error.details[0].message);
    }
};

const doOrder = async (req, res) => {
    try {
        const { products, quantities, payMethod, status, comments } = req.body;

        if(products && quantities && payMethod && status && comments){
            const n = quantities.length;
            const price = await Price(n, products, quantities);

            const add = await Order.findById(req.params.id);
            add.orders.push({...req.body, price});

            await add.save();
            res.status(201).json({message: "Order created"});
        } else{
            res.status(401).send({message: "Some data are missing"});
        }
    } catch (error) {
        return res.status(500).json(error.details[0].message);
    }
};

const updateOrder = async (req, res) => {
    try {
        const { products, quantities, payMethod, status, comments } = req.body;

        if(products && quantities && payMethod && status && comments){
            const n = quantities.length;
            const price = await Price(n, products, quantities);
            const { id } = req.params;

            const add = await Order.findById(id);
            add.orders.splice(0,2);
            add.orders.push({...req.body, price});

            await add.save();
            res.status(201).json({message: "Order updated"});
        } else{
            res.status(401).send({message: "Some data are missing"});
        }
    } catch (error) {
        return res.status(500).json(error.details[0].message);
    }
};

const deleteOrder = async (req, res) => {
    try {
        const { id } = req.params;
        await Order.findByIdAndDelete(id);
        res.status(201).json({message: "Order deleted"});
    } catch (error) {
        return res.status(500).json(error.details[0].message);
    }
};

module.exports = { AllOrders, newOrder, doOrder, updateOrder, deleteOrder };