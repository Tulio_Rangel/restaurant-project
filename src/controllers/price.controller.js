const Product = require("../models/product.model");

async function Price(n, products, quantities){
    const food = await Product.find({productName: {$in: products}});
    const costs = food.map(cost => cost.price);

    let prices=[];
    for (let i = 0; i < n; i++) {
        let p = costs[i];
        prices.push(p);
    }

    let price = 0;
    for (let m = 0; m < n; m++) {
        let q = quantities[m]*prices[m];
        price = price+q
    }
    return price;
};

module.exports = { Price };