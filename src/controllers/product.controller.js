const Product = require("../models/product.model");
const clientRedis = require("../config/redis");

const CeroProducts = async (req, res) => {
    try {
        const count = await Product.estimatedDocumentCount();
        if (count > 0) return;

        const initialProducts = Promise.all([
            new Product ({productName: "Roast Chicken", description: "Chicken that is roasted", price: 15000}).save(),
            new Product ({productName: "Mineral Water", description: "Mineral Water", price: 5000}).save(),
            new Product ({productName: "Tiramisú", description: "Italian dessert", price: 8000}).save()
        ]);
        res.json(initialProducts);
    } catch (error) {
        return res.status(500).json(error.details[0].message);
    }
};

const AllProducts = async (req, res) => {
    try {
        const arrayProducts = await Product.find();
        clientRedis.setex('products', 60 * 5, JSON.stringify(arrayProducts));
        res.status(200).json(arrayProducts);
    } catch (error) {
        return res.status(500).json(error.details[0].message);
    }
};

const NewProduct = async (req, res) => { 
    try {
        const { productName, description, price } = req.body;
        if (productName && description && price){
            const sameProduct = Product.findOne({ productName });
            if (sameProduct){
                res.status(400).json({message: "The product already exists"});
            } else{
                new Product({...req.body}).save();
                res.status(201).json({message: "Successfully created"})
            }
        } else{
            res.status(204).json({message: "Some data are missing"});
        }
    } catch (error) {
        return res.status(500).json(error.details[0].message);
    }
};

const UpdateProduct = async (req, res) => {
    try {
        const { productName, description, price } = req.body;
        if (productName && description && price){
            const { id } = req.params;
            const updates = {...req.body};
            const options = { new: true };
            await Product.findByIdAndUpdate(id, updates, options);
            clientRedis.del('products');
            res.status(200).json({message: "The product was updated"});
        } else{
            res.status(204).json({message: "Some data are missing"});
        }
    } catch (error) {
        return res.status(500).json(error.details[0].message);
    }
};

const DeleteProduct = async (req, res) => {
    try {
        const { id } = req.params;
        if (id){
            await Product.findByIdAndDelete(id);
            res.status(200).json({message: "The product was deleted"});
        } else{
            res.status(204).json({message: "Some data are missing"});
        }
    } catch (error) {
        return res.status(500).json(error.details[0].message);
    }
};

module.exports = { CeroProducts, NewProduct, UpdateProduct, DeleteProduct, AllProducts };