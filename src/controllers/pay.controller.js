const Pay = require("../models/paymethod.model");

const CeroPayMethod = async (req, res) => {
    try {
        const count = await Pay.estimatedDocumentCount();
        if (count > 0) return;

        const initialPayMethods = Promise.all([
            new Pay ({payName: "Cash"}).save(),
            new Pay ({payName: "Credit Card"}).save(),
            new Pay ({payName: "Debit Card"}).save()
        ]);
        res.json(initialPayMethods);
    } catch (error) {
        return res.status(500).json(error.details[0].message);
    }
};

const AllPayMethods = async (req, res) => {
    try {
        const arrayPayMethods = await Pay.find();
        res.status(200).json(arrayPayMethods);
    } catch (error) {
        return res.status(500).json(error.details[0].message);
    }
};

const NewPayMethod = async (req, res) => {
    try {
        const { payName } = req.body;
        if (payName){
            const samePayMethod = Pay.findOne({ payName });
            if (samePayMethod){
                res.status(400).json({message: "The pay method already exists"});
            } else{
                new Pay({...req.body}).save();
                res.status(201).json({message: "Successfully created"})
            }
        } else{
            res.status(204).json({message: "Some data are missing"});
        }
    } catch (error) {
        return res.status(500).json(error.details[0].message);
    }
};

const UpdatePayMethod = async (req, res) => {
    try {
        const { payName } = req.body;
        if (payName){
            const { id } = req.params;
            const updates = {...req.body};
            const options = { new: true };
            await Pay.findByIdAndUpdate(id, updates, options);
            res.status(200).json({message: "The pay method was updated"});
        } else{
            res.status(204).json({message: "Some data are missing"});
        }
    } catch (error) {
        return res.status(500).json(error.details[0].message);
    }
};

const DeletePayMethod = async (req, res) => {
    try {
        const { id } = req.params;
        if (id){
            await Pay.findByIdAndDelete(id);
            res.status(200).json({message: "The pay method was deleted"});
        } else{
            res.status(204).json({message: "Some data are missing"});
        }
    } catch (error) {
        return res.status(500).json(error.details[0].message);
    }
};

module.exports = { CeroPayMethod, AllPayMethods, NewPayMethod, UpdatePayMethod, DeletePayMethod };