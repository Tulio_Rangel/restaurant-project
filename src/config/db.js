const mongoose = require("mongoose");
const port = process.env.PORT || 3000;

const urlDB = `mongodb+srv://${process.env.USERDB}:${process.env.PASSDB}@cluster0.ty3yp.mongodb.net/${process.env.DBNAME}?retryWrites=true&w=majority`;

(async () => {
    const db = await mongoose.connect(urlDB, {useNewUrlParser: true, useUnifiedTopology: true})
    .then(() => console.log("DB is connected"))
    .catch(e => console.log(e))
})();