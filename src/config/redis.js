const redis = require('redis');

const clientRedis = redis.createClient(6379);

module.exports = clientRedis;